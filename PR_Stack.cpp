#include <iostream>
#include <string>
using namespace std;
// класс стэка
class Stack {
private:
    int size;
    char *stack;
public:
    Stack() {
        size = 0;
        stack = NULL;
    }
    ~Stack() {
        if (stack) {
            delete[] stack;
        }
    }
    void add(char value);
    void print();
    void remove();
    bool empty();
    char getLast();
};
// функция проверяющая правильность скобок
bool checkBrackets(string &s) {
    Stack brackets;
    for (int i = 0; i < s.length(); i++) {
        if (s[i] == '(') {
            brackets.add(')');
        }
        else if (s[i] == '{') {
            brackets.add('}');
        }
        else if (s[i] == '[') {
            brackets.add(']');
        }
        else {
            if (s[i] == ')' || s[i] == ']' || s[i] == '}') {
                if (brackets.empty()) {
                    return false;
                }
                else {
                    if (brackets.getLast() == s[i]) {
                        brackets.remove();
                    }
                    else {
                        return false;
                    }
                }
            }
        }
    }
    return brackets.empty();
}
// основная функция
int main() {
    string input;
    cout << "Input string: ";
    getline(cin, input);
    if (checkBrackets(input)) {
        cout << "True" << endl;
    }
    else {
        cout << "False" << endl;
    }
    return 0;
}
// добавить в стэк
void Stack::add(char value) {
    char *save = stack;
    size++;
    stack = new char[size];
    for (int i = 0; i < size - 1; i++) {
        stack[i] = save[i];
    }
    stack[size - 1] = value;
}
// распечатать стэк
void Stack::print() {
    for (int i = 0; i < size; i++) {
        cout << stack[i] << ' ';
    }
    cout << endl;
}
// удалить элемент из стэка
void Stack::remove() {
    char *save = stack;
    size--;
    stack = new char[size];
    for (int i = 0; i < size; i++) {
        stack[i] = save[i];
    }
}
// проверка пуст ли стэк
bool Stack::empty() {
    return size == 0;
}
// получить крайний элемент стэка
char Stack::getLast() {
    return stack[size - 1];
}
