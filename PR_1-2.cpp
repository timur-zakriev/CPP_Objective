// Практическая работа 1
// Программа работает с введенной датой
#include <iostream>
#include <iomanip>
using namespace std;
// структура для хранения даты
struct date {
  int day, month, year;
  // методы для структуры
  int lastNumber();
  bool checkDate();
  int   countDay();
  int    dayWeek();
  int   friday13();
  void   setDate();
  void  showDate();
  int   distance();
  void    addDay(int plus);
  void    subDay(int sub);
};
// возвращает количество дней в месяце
int date::lastNumber() {
  switch (month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:  return 31;
              break;
    case 4:
    case 6:
    case 9:
    case 11:  return 30;
              break;
    case 2:
              if(year % 4 == 0){
                if(year % 400 != 0 && year % 100 == 0){
                  return 28;
                } else {
                  return 29;
                }
              } else {
                return 28;
              }
    }
    return -1;
}
int date::countDay(){ // количество дней с начала эры
    int count = 0; // хранит количество дней
    int saveMonth = month; // хранит изначальное количество месяцев
    for(int y = 1; y < year; y++){ // считает количество дней до года даты
        if((y % 4 == 0 && y % 100 != 0) || y % 400 == 0){ // проверяет високосный ли год
            count += 366;
        } else {
            count += 365;
        }
    }
    for(int m = 0; m < saveMonth; m++){ // добавляет количество дней до месяца даты
        month = m;
        count += lastNumber(); // берет количество дней из функции
    }
    month = saveMonth; // возвращает изначальное значение месяца
    count += day; // добавляет оставшиеся дни
    return count;
}
// вычисляет день недели
int date::dayWeek() {
  int a = countDay() / 7;
  int b = a * 7;
  int c = countDay() - b;
  return c;
}
// проверяет корректность введенной даты
bool date::checkDate() {
  if(year > 0){ // положительное ли число год
    if(month < 13 && month > 0){ // положительно ли число месяц и не больше 12
      if(day > 0 && day <= lastNumber()){ // положительное ли число день и не больше количества дней в месяце
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  } else {
    return false;
  }
}
// считает кол-во дней до пятницы 13
int date::friday13() {
    date newDate;
    newDate.day = day;
    newDate.month = month;
    newDate.year = year;
    int count = 0;
    while (true) {
        if (newDate.dayWeek() == 4 && newDate.day == 13) {
            return count;
        }
        newDate.day++;
        count++;
        if (newDate.day > newDate.lastNumber()) {
            newDate.month++;
            newDate.day = 1;
            if (newDate.month > 12) {
                newDate.month = 1;
                newDate.year++;
            }
        }
    }
    return count;
}
// устанавливает дату
void date::setDate() {
    cout << "Input date (day month year): ";
    cin >> day >> month >> year;
}
// показывает дату
void date::showDate() {
    cout << "Date: "
         << setfill('0') << setw(2) << day   << '.'
         << setfill('0') << setw(2) << month << '.' << year << endl;
}
// считает количество дней между двумя датами
int date::distance() {
    date newTime;
    newTime.setDate();
    return abs(newTime.countDay() - countDay());
}
// увеличивает дату на указанное количество дней
void date::addDay(int plus) {
    day += plus;
    while (day > lastNumber()) {
        month++;
        day -= lastNumber();
        if (month > 12) {
            year++;
            month -= 12;
        }
    }
}
// уменьшает дату на указанное количество дней
void date::subDay(int sub) {
    if (sub > countDay()) {
        cout << "Error input!" << endl;
    }
    day -= sub;
    while (day < 1) {
        month--;
        day += lastNumber();
        if (month < 1) {
            year--;
            month += 12;
        }
    }
}
// основная функция
int main(){
  date today; // определяет структурную переменную today
  today.setDate();
  if(!today.checkDate()){ // проверка правильности ввода
    cout << "Error input!\n";
    return 0;
  } else {
    today.showDate();
    cout << "Number of days in a month : " << today.lastNumber() << endl;
    cout << "Count days : "                <<   today.countDay() << endl;
    cout << "Day of week : ";
    switch (today.dayWeek()) {
        case 0: cout << "Monday"     << endl;
                break;
        case 1: cout << "Tuesday"    << endl;
                break;
        case 2: cout << "Wednesday"  << endl;
                break;
        case 3: cout << "Thursday"   << endl;
                break;
        case 4: cout << "Friday"     << endl;
                break;
        case 5: cout << "Saturday"   << endl;
                break;
        case 6: cout << "Sunday"     << endl;
                break;
    }
    cout << "Friday 13: "            <<   today.friday13() << endl;
  }
  int distance = today.distance();
  // вывод количества дней между датами
  cout << "Distance: " << distance << endl;
  // переменная для увеличения даты
  int plusDay;
  cout << "Input plus day: ";
  cin  >> plusDay;
  // подсчет и вывод увеличения
  today.addDay(plusDay);
  today.showDate();
  // переменная для уменьшения даты
  int sub;
  cout << "Input sub day: ";
  cin  >> sub;
  // подсчет и вывод уменьшения
  today.subDay(sub);
  today.showDate();
  cout << "It's test!" << endl;
  return 0;
}
