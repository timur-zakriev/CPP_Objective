// Практическая работа 5
// программа считающая необходимое количество плитки
#include <iostream>
#include <cmath>
using namespace std;
// структура для работы с данными
struct Area {
  int metrs, sm;
  bool check();
};
// проверка введенных данных
bool Area::check() {
  return metrs >= 0 && sm >= 0;
}
// считает сколько упоковок нужно купить (24 плитки в упаковке)
double plate(Area lenght, Area width, int size) {
	float packages;
  float len = lenght.metrs + lenght.sm / 100.;
  float wid = width.metrs  + width.sm  / 100.;
  float plate;

  len = ceil(len / size);
  wid = ceil(wid / size);
  plate = len * wid;

  cout << "Plate: " << plate << endl;
  packages = plate / 24;
	return packages;
}

int main() {
  int size;
  Area lenght;
  Area width;
  cout << "Input lenght room: ";
  cin  >> lenght.metrs >> lenght.sm;
  cout << "Input width room: ";
  cin  >> width.metrs >> width.sm;
  cout << "Input plate size (1/2/3):";
  cin  >> size;
  // защита от неправильного ввода
  if (!lenght.check() || !width.check() || (size <= 0 && size >= 3)) {
    cout << "Error input!" << endl;
    return 0;
  } else {
    float packages = plate(lenght, width, size);
    cout << "Packages you need: " << ceil(packages) << endl;
  }
  return 0;
}
