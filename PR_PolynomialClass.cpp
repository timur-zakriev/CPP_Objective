#include <iostream>
#include <cmath>
using namespace std;

class multinomial {
private:
    int a[100];
    int n;
public:
    void input();
    void output();
    double calculate();
    multinomial add(multinomial mB);
    multinomial deduct(multinomial mB);
    multinomial multiplication(multinomial mB);
    int getN();
    int getA(int value);
    void setN(int n);
    void setA(int index, int value);
};

int main() {
    int menu = -1;
    multinomial first;
    multinomial second;
    multinomial third;
    while (menu != 0) {
        cout << endl;
        cout << "1 - input first multinomial."   << endl;
        cout << "2 - input second multinomial."  << endl;
        cout << "3 - output first multinomial."  << endl;
        cout << "4 - output second multinomial." << endl;
        cout << "5 - sum first and second."      << endl;
        cout << "6 - deduct first and second."   << endl;
        cout << "7 - multiply first and second." << endl;
        cout << "8 - calculate."                 << endl;
        cout << "0 - exit."                      << endl;
        cout << "Input functions:";
        cin  >> menu;
        switch (menu) {
            case 1:
                    first.input();
                    break;
            case 2:
                    second.input();
                    break;
            case 3:
                    first.output();
                    break;
            case 4:
                    second.output();
                    break;
            case 5:
                    third = first.add(second);
                    cout << "f(x) = ";
                    third.output();
                    cout << endl;
                    break;
            case 6:
                    third = first.deduct(second);
                    cout << "f(x) = ";
                    third.output();
                    cout << endl;
                    break;
            case 7:
                    third = first.multiplication(second);
                    cout << "f(x) = ";
                    third.output();
                    cout << endl;
                    break;
            case 8:
                    cout << "What calculate :" << endl;
                    cout << "1 - first."       << endl;
                    cout << "2 - second."      << endl;
                    cout << "3 - third."       << endl;
                    cin  >> menu;
                    double value;
                    switch (menu) {
                        case 1:
                                value = first.calculate();
                                cout << "Value multinomial = " << value << endl;
                                break;
                        case 2:
                                value = second.calculate();
                                cout << "Value multinomial = " << value << endl;
                                break;
                        case 3:
                                value = third.calculate();
                                cout << "Value multinomial = " << value << endl;
                                break;
                    }
                    break;
        }
    }
    return 0;
}

void multinomial::input() {
    cout << "Input nominal multinomial: ";
    cin  >> n;
    cout << "Input coefficient: " << endl;
    for (int k = 0; k < 100; k++) {
        a[k] = 0;
    }
    for (int i = 0; i < n; i++) {
        cout << "a(" << i << ") = ";
        cin  >> a[i];
    }
}

void multinomial::output() {
    int f = 0;
    for (int i = n; i >= 0; i--)
        if (a[i] != 0) {
            if (f == 0) {
                if (i != 0)
                    cout << a[i] << "*x^" << i;
            else
                cout << a[i];
                f++;
            }
    else {
        if(i != 0)
            if(a[i] > 0)
                cout << "+" << a[i] << "*x^" << i;
            else
                cout << a[i] << "*x^" << i;
        else
            if(a[i] > 0)
                cout << "+" << a[i];
            else
                cout << a[i];
            f++;
        }
    }
    if (f == 0){
        cout << '0';
    }
    cout << endl;
}

double multinomial::calculate() {
    double x, v, vx;
    v = 0;
    cout << "Input X: ";
    cin  >> x;
    for (int i = 0; i <= n; i++) {
        v = v + a[i] * vx;
        vx = vx * x;
    }
    return v;
}

multinomial multinomial::add(multinomial mB) {
    multinomial add;
    for (int k = 0; k < 100; k++) {
        add.setA(k, 0);
    }
    if (this->n > mB.getN()) {
        add.setN(this->n);
    }
    else {
        add.setN(mB.getN());
    }
    for (int i = 0; i <= n; i++) {
        add.setA(i, this->a[i] + mB.getA(i));
    }
    return add;

}

multinomial multinomial::deduct(multinomial mB) {
    multinomial deduct;
    for (int k = 0; k < 100; k++) {
        deduct.setA(k, 0);
    }
    if (this->n > mB.getN()) {
        deduct.setN(this->n);
    }
    else {
        // n = mB.n;
        deduct.setN(mB.getN());
    }
    for (int i = 0; i <= n; i++) {
        deduct.setA(i, this->a[i] - mB.getA(i));
    }
    return deduct;
}

multinomial multinomial::multiplication(multinomial mB) {
    multinomial multy;
    for (int k = 0; k < 100; k++) {
        multy.setA(k, 0);
    }
    multy.setN(this->n + mB.getN());
    for (int i = 0; i <= this->n; i++) {
        for (int j = 0; j <= mB.getN(); j++) {
            multy.setA(i + j, multy.getA(i + j) + this->a[i] * mB.getA(j));
        }
    }
    return multy;
}


int multinomial::getN() {
    return this->n;
}

int multinomial::getA(int value) {
    return this->a[value];
}

void multinomial::setN(int n) {
    this->n = n;
}

void multinomial::setA(int index, int value) {
    this->a[index] = value;
}
