#include <iostream>
using namespace std;

class Queue {
private:
    int size;
    int *queue;
public:
    Queue() {
        size = 0;
        queue = NULL;
    }
    ~Queue() {
        if (queue){
            delete[] queue;
        }
    }
    void add(int value);
    void show();
    void remove();
};

int main() {
    Queue myQueue;
    int menu;
    do {
        cout << endl << endl;
        cout << "1 - add element"    << endl;
        cout << "2 - remove element" << endl;
        cout << "3 - show queue"     << endl;
        cout << "0 - exit"           << endl;
        cout << "Input menu: ";
        cin >> menu;
        switch (menu) {
            case 1:
                    cout << "Input element: ";
                    cin  >> menu;
                    myQueue.add(menu);
                    break;
            case 2:
                    myQueue.remove();
                    break;
            case 3:
                    cout << "Queue: ";
                    myQueue.show();
                    break;
        }
    } while(menu != 0);
    return 0;
}

void Queue::add(int value) {
    int *save = queue;
    size++;
    queue = new int[size];
    for (int i = 0; i < size - 1; i++) {
        queue[i] = save[i];
    }
    queue[size - 1] = value;
}

void Queue::show() {
    if (queue){
        for (int i = 0; i < size; i++) {
            cout << queue[i] << ' ';
        }
        cout << endl;
    }
    else {
        cout << "Queue is NULL!" << endl;
    }
}

void Queue::remove() {
    if (!queue) {
        cout << "Queue is NULL!" << endl;
    }
    else {
        int *save = queue;
        size--;
        queue = new int[size];
        for (int i = 0; i < size; i++) {
            queue[i] = save[i + 1];
        }
    }
}
